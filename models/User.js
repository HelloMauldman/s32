const mongoose = require("mongoose");

let userSchema = new mongoose.Schema(
	{
		firstName:{
			type:String,
			require: [true, "First Name is required"]

		},
		lastName: {
			type:String,
			require: [true, "Last Name is required"]
		},
		email:{
			type:String,
			required: [true, "Email is required."]
		},
		password: {
			type:String,
			required: [true, "Password is requrie"]
		},
		mobileNo:{
			type: String,
			required: false
		},
		age:{
			type:Number,
			required: [true, "Age is required."]
		},
		isAdmin:{
			type:Boolean,
			default:false
		},
		enrollment:[
			{
				courseId:{
					type:String,
					required:[true, "Course ID is required."]
				},
				enrolledOn:{
					type: Date,
					default: new Date()
				},
				status:{
					type:String,
					default: "Enrolled"
				}
			}
		]
	}
)

module.exports = mongoose.model("User", userSchema);


