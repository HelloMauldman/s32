const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes")


const app = express();
const port = 3002;

//middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));
//






app.use("/users", userRoutes)

mongoose.connect("mongodb+srv://admin:admin1234@zuitt-bootcamp.exnj7.mongodb.net/courseBooking?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology:true
});


const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", ()=>{console.log("We're connected.")})

app.listen(port,()=>{
	console.log(`Server running at port ${port}`);
})